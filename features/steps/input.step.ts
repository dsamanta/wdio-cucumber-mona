import { Given, When, Then } from '@wdio/cucumber-framework';
import inputPage from "../pages/inputPage"

// Given(/^I am on the input page$/, async(path) => {
// 	await inputPage.open(path);
// });

// When(/^I land with on the input page I should see the "([^"]*)" title$/, async(args1) => {
// 	await inputPage.titleValidation(args1);
// });

// Then(/^I should enter "([^"]*)" in the inputbox.$/, async (args1) => {
// 	console.log(args1);
// });


Given(/^I am on the "([^"]*)" page$/, async (path) => {
	await inputPage.open(path);
});

When(/^I land with on the input page I should see the "([^"]*)" title$/, async (title) => {
	await inputPage.titleValidation(title);
});

Then(/^I should enter "([^"]*)" in the inputbox.$/, async(number) => {
	await inputPage.enteringInputBox(number);
});

