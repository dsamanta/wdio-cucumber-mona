import { Given, When, Then } from '@wdio/cucumber-framework';
import dropdownPage from "../pages/dropdownPage";

Given(/^I am on the "([^"]*)" page$/, async (path) => {
	await dropdownPage.open(path)
});

When(/^I land with on the dropdown page I should see the default option as "([^"]*)"$/, async (args1) => {
	await dropdownPage.visibilityDefualtOption();

});

Then(/^I should select "([^"]*)" from the dropdown list.$/, async (number) => {
	await dropdownPage.selectDropdown(number)
});

