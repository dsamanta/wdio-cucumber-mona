import { Given, When, Then } from '@wdio/cucumber-framework';
import DropDownPage from "../pages/dropdownPage";

Given(/^I am on the "([^"]*)" page$/, async (path) => {
	await DropDownPage.open(path)
});

When(/^I land with on the dropdown page I should see the default option as "([^"]*)"$/, async (args1) => {
	await DropDownPage.visibilityDefualtOption();

});

Then(/^I should select "([^"]*)" from the dropdown list.$/, async (number) => {
	await DropDownPage.selectDropdown(number)
});

