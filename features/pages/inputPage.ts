import Page  from "../pages/page"

class inputPage extends Page{


    get  inputPageTitleElement() {return $("h3")} 
    get inputBoxElement(){return $("input[type='number']")} 

    async open(path){
        await super.open(path)
        }

            async titleValidation(args){
                await expect(await this.inputPageTitleElement).toHaveTextContaining(args);
            }

    //addValue method don't clear the value ,it only enter the values.
            async enteringInputBox(number){
                await this.inputBoxElement.setValue(number)
        
            }

    }
        export default new inputPage();
