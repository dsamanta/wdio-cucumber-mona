import Page  from "../pages/page"
const { expect } = require('chai');

class DropDownPage extends Page{


    get  defaultOptionEle() {return $("#dropdown")} 
    get option1Ele(){return $("option[value='1']")}
    get option2Ele(){return $("option[value='2']")} 


    async open(path){
        await super.open(path)
        }

//Validating default option been selected from the dropdown 
            async visibilityDefualtOption(){
                let InnerText;
                InnerText = await this.defaultOptionEle.getText();
                //await expect (InnerText).toHaveTextContaining('Please select an option');
                await expect(InnerText).to.include('Please select an option');
                //await expect(InnerText).toHaveTextContaining('Please select an option');
            }

    //Selecting the options from the dropdown list
            async selectDropdown(option){

               if (option === 1) {
                await (this.option1Ele).click();
               } else{
                   await (this.option2Ele).click();
               }

               await browser.pause(30000);
        
            }

    }
        export default new DropDownPage();
